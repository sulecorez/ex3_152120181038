#include "Triangle.h"
#include "Square.h"
#include "Circle.h"
#include <iostream>
using namespace std;
using namespace Student1;
/*
 * Question1: Fix the code and compile it */
void test();
int main() {
	/******************/
	/*DO NOT CHANGE any line of code in this function */
	Triangle triangle(3, 5, 6);
	triangle.setA(7);
	triangle.setB(8);
	triangle.setC(9);
	double triAngleCircumference = triangle.calculateCircumference();

	Circle circle(3);
	double circleCircumference1 = circle.calculateCircumference();
	circle.setR(5);
	double circleCircumference2 = circle.calculateCircumference();
	double circleArea = circle.calculateArea();

	Square square(3);
	double squareCircumference1 = square.calculateCircumference();
	square.setA(4);
	square.setB(5);
	double squareCircumference2 = square.calculateCircumference();
	double squareArea = square.calculateArea();
	/******************/
	test();
	return 0;
}

void test() {
	const Circle circle1(30);

	/* Question2: Block the changing r of above object not other objects only above object.*/
	//I did circle1 as constant class and also its member functions (getR, calculateCircumference) in header and cpp file.

	//circle1.setR(20); //This line must show compile error.
	double circumference = circle1.calculateCircumference();
	circle1.getR();

	/*DO NOT REMOVE below code block */
	{
		Circle circle2(30);
		circle2.setR(20);
		double circumference2 = circle2.calculateCircumference();
		circle2.getR();
	}
	/*End of code block*/

	Circle circle4(5);
	circle4.setR(10);
	Circle circle5(10);
	/* Question3: In Circle class, create an equals function which returns boolean to compare circle4 and circle5 objects
	 * and print if they are equal or not. */

	 //To comparing 2 class, we have to create another class named as "compare".
	Circle compare(10);
	bool isEqual = compare.equals(circle4, circle5);
	if (isEqual == 1)
	{
		cout << "Radius' of circle 4 and 5 are equal\n";
	}
	else
	{
		cout << "Radius' of circle 4 and 5 are not equal\n";
	}

	/* Question4: Review the PI variable and make it unchangeable from anywhere of that program.
	 * You know PI is always 22/7 */
	 // I changed the PI value as constant value in Circle.h and it is now unchangeable.

	  /* Question5: Overload the setR method of Circle class to take integer values */
}

/*
 * Question6: Review the code and fix the bugs
 */
